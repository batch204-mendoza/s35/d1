// npm init - y set up node? without specifiying details
// npm install express mongoose- set up mongoose dependencies

/*
mini activity-:
create an express js server designnated to port 4000
create a new route  with endpoint / hello and method get
- should be able to respond with "world"

*/

const express= require("express");
const app = express();
//Mongoose is a package that allows creation of schemas to model our data structures (setting blueprints)
// also has acccss to a number of methods for manipulating our database
const mongoose = require("mongoose");

// mongoDB connection
/* 

syntax:
mongoose.conect("Mongo DB atlas connection string", {
	useNewUrlParser:true,
	useUnifiedTopology: true
})
*/

//"mongodb+srv://Jack:admin123@cluster0.o4i73.mongodb.net/<DATABASE NAME>?retryWrites=true&w=majority"

mongoose.connect("mongodb+srv://Jack:admin123@cluster0.o4i73.mongodb.net/B204-to-do?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology: true
});


// set notifications for connection success or failure
let db = mongoose.connection;
// if a connection error occured, out in the console
db.on("error", console.error.bind(console, "Connection Error"));


// if a connection is good occured, output in the console
db.once("open", () => console.log(" We're connected to the cloud database"));

//mongoose schema
//schemas determine the structure of the documents to be written in the database
//schemas act as blueprints to our data

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// models- for instantiating
// first parameter of the mongoose.model method ("Task") in this case of the
// 2nd parameter is used to specify the schema/blueprint of the documents that will be stored in the Mongodb collection
const Task = mongoose.model("Task", taskSchema);



app.use(express.json());

const port =4000;
app.get("/hello", (req, res)=>{
		res.send("Hello World");

});

// route to create a task
app.post("/tasks", (req,res) =>{
	Task.findOne({name: req.body.name}, (err,result)=>{
		if (result !==null && result.name == req.body.name) {
			return res.send("duplicate Task Found");
		}
		else {
			let newTask= new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				}
				else{
					return res.status(201).send("new task created")
				}
			})
		}
	})

})

// get all the tasks in our b204-to-do database
// can have same urls as long as methods are different
app.get("/tasks", (req,res) =>{
Task.find({}, (err,result) =>{
	if (err){
		return console.log(err);
	} else{
		return res.status(200).json({
			data: result
		})
	}
})
})

	
const userSchema = new mongoose.Schema({
	username: String,
	password: String
	
})
const User = mongoose.model("User", userSchema);  	

app.post("/signup", (req,res) =>{  
	User.findOne({username: req.body.username}, (err,result)=>{ 
		if (result !==null && result.username == req.body.username) { 
			return res.send("username already exists "); 
		} 
		else { 
			let newUser= new User({ 
				username: req.body.username, 
				password: req.body.password
			}); 
			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr)
				}
				else{
					return res.status(201).send("new user registered")
				}
			})
		}
	})

})

app.listen(port, () =>console.log(`server is running at port: ${port}`));
















